/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>

#include "BaseApplication.h"
#include "SelectionBox.h"

//---------------------------------------------------------------------------

class TutorialApplication : public BaseApplication
{
public:
	// C++ application constructor
	TutorialApplication(void);

	// C++ application destructor
	virtual ~TutorialApplication(void);

private:
	// Function to fill the scene with graphic objects
	virtual void createScene(void);

	// Function to initialize screen overlay objects
	virtual void createFrameListener(void);

	// Function to delete scene objects
	virtual void destroyScene();

	// Function to update the scene every frame
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& fe);

	// Function to process keyboard input
	virtual bool keyPressed(const OIS::KeyEvent& ke);

	// Function to process keyboard input
	virtual bool keyReleased(const OIS::KeyEvent& ke);
 
	// Function to process mouse input
	virtual bool mouseMoved(const OIS::MouseEvent& me);

	// Function to process mouse input
	virtual bool mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id);

	// Function to process mouse input
	virtual bool mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id);

	// Function to set the final position of the bounding box and select robot(s)
	void performSelection(const Ogre::Vector2& first, const Ogre::Vector2& second);

	// Function to hide the bounding boxes of previously selected robots
	void deselectObjects();

	// Function to show the bounding boxes of newly selected robot(s)
	void selectObject(Ogre::MovableObject* obj);

	// Call function to switch the top and bottom, left and right locations of the selection box
	static void swap(float& x, float& y);

	// Pointer to scene query that encompasses a large part of the screen
	Ogre::PlaneBoundedVolumeListSceneQuery* mVolQuery;

	// Bounding box used to query and select robots within the scene
	SelectionBox* mSelectionBox;

	// Flag for indicating the setting the selecting box
	bool bSelecting;

	// Startingt and stopping points for the selecting box
	Ogre::Vector2 mStart, mStop;

	// List of robot objects that have been selected by the selecting box
	std::list<Ogre::MovableObject*> mSelected;

};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------
