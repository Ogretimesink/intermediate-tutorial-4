/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
	: bSelecting(false)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
	// Destroy the volume query
	mSceneMgr->destroyQuery(mVolQuery);
 
	// Check for the selection box
	if(mSelectionBox)
	{
		// Delete the selection box
		delete mSelectionBox;
	}
}
//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue(1.0f, 1.0f, 1.0f));
 
	// Loop to place 100 robots on the screen in a tiled formation
	for(int i = 0; i < 10; ++i)
	{
		for(int j = 0; j < 10; ++j)
		{
			// Create an instance of a computer graphic model robot entity
			Ogre::Entity* ent = mSceneMgr->createEntity("Robot" + Ogre::StringConverter::toString(i+j*10), "robot.mesh");

			// Create a new node object in the scene for the robot entity
			Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(i * 15, 0, j * 15));

			// Add the robot entity to the node
			node->attachObject(ent);

			// Set the size of the robot entity node
			node->setScale(0.1f, 0.1f, 0.1f);
		}
	}
 
	// Position the camera
	mCamera->setPosition(-60, 100, -60);

	// Look at the position with the camera
	mCamera->lookAt(60, 0, 60);

	// Declare a bounding box used to query and select robots within the scene
	mSelectionBox = new SelectionBox("SelectionBox");

	// Initialize the bounding box
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(mSelectionBox);
 
	// Initialize the volume query
	mVolQuery = mSceneMgr->createPlaneBoundedVolumeQuery(Ogre::PlaneBoundedVolumeList());
}
//---------------------------------------------------------------------------
void TutorialApplication::createFrameListener(void)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::createFrameListener();

	// Show the screen cursor by default
	mTrayMgr->showCursor();
}
//---------------------------------------------------------------------------
void TutorialApplication::destroyScene(void)
{

}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
	// Call the BaseApplication virtual function because we still want its functionality
	bool ret = BaseApplication::frameRenderingQueued(fe);

	return ret;
}

//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyPressed(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyReleased(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyReleased(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseMoved(const OIS::MouseEvent& me)
{
	// Send mouse moved to the tray manager that controls the screen cursor movement
	mTrayMgr->injectMouseMove(me);

	// Check if the bounding box has been set
	if (bSelecting)
	{
		// Set ray to mouse location
		Ogre::Ray MouseRay = mTrayMgr->getCursorRay(mCamera);

		// Origin of the mouse click on the screen
		Ogre::Vector3 MouseOrigin = MouseRay.getOrigin();

		// Get the vector of the cameras frustum
		Ogre::Vector3 MouseOriginViewMatrix	= mCamera->getViewMatrix(true) * (MouseOrigin);

		// Get the vector of the camera to screen
		Ogre::Vector3 MouseOriginProjectionMatrix = mCamera->getProjectionMatrix() * MouseOriginViewMatrix;

		// Find the relative location of the mouse cursor on the 2D screen
		mStop.x = ((MouseOriginProjectionMatrix.x + 1.0f) * 0.5f);

		// Find the relative location of the mouse cursor on the 2D screen
		mStop.y = (1.0f - (MouseOriginProjectionMatrix.y + 1.0f) * 0.5f);

		// Call the function to set the points of the bounding box
		mSelectionBox->setCorners(mStart, mStop);
	}

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Set ray to mouse location
	Ogre::Ray MouseRay = mTrayMgr->getCursorRay(mCamera);

	// Origin of the mouse click on the screen
	Ogre::Vector3 MouseOrigin = MouseRay.getOrigin();

	// Get the vector of the cameras frustum
	Ogre::Vector3 MouseOriginViewMatrix	= mCamera->getViewMatrix(true) * (MouseOrigin);

	// Get the vector of the camera to screen
	Ogre::Vector3 MouseOriginProjectionMatrix = mCamera->getProjectionMatrix() * MouseOriginViewMatrix;

	// Find the relative location of the mouse click position on the 2D screen
	mStart.x = ((MouseOriginProjectionMatrix.x + 1.0f) * 0.5f);

	// Find the relative location of the mouse click position on the 2D screen
	mStart.y = (1.0f - (MouseOriginProjectionMatrix.y + 1.0f) * 0.5f);

	// Set the bounding box ending position
	mStop = mStart;
 
	// Set flag to indicate the bounding box selecting robots
	bSelecting = true;

	// Delete the robots that may be making up the contents of the selection box
	mSelectionBox->clear();

	// Show the selection box on the screen
	mSelectionBox->setVisible(true);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Call the function to set the final position of the selection box and select robot(s)
	performSelection(mStart, mStop);

	// Set the flag to indicate the selection box no longer selects robots
	bSelecting = false;

	// Do not show the selection box
	mSelectionBox->setVisible(false);

	return true;
}
//---------------------------------------------------------------------------
void TutorialApplication::performSelection(const Ogre::Vector2& first, const Ogre::Vector2& second)
{
	// Declare variables to indicate the location of the left and right sides bounding box
	float left = first.x, right = second.x;

	// Declare variables to indicate the location of the left and right sides bounding box
	float top = first.y, bottom = second.y;

	// Check if the left and right side locations
	if(left > right)
	{
		// Call function to switch the left and right side locations of the selectio box
		swap(left, right);
	}

	// Check if the top and bottom locations
	if(top > bottom)
	{
		// Call function to switch the top and bottom locations of the selectio box
		swap(top, bottom);
	}

	// Check the size of the box
	if((right - left) * (bottom - top) < 0.0001)
	{
		// Quit the robot selection process if the box doesn't meet size requirement
		return;
	}

	// Make a camera ray at the top left point on the selection box
	Ogre::Ray topLeft = mCamera->getCameraToViewportRay(left, top);

	// Make a camera ray at the top right point on the selection box
	Ogre::Ray topRight = mCamera->getCameraToViewportRay(right, top); 

	// Make a camera ray at the bottom left point on the selection box
	Ogre::Ray bottomLeft = mCamera->getCameraToViewportRay(left, bottom);

	// Make a camera ray at the bottom right point on the selection box
	Ogre::Ray bottomRight = mCamera->getCameraToViewportRay(right, bottom);

	// Declare a 3 dimensional cube volume to initialize the points on the selection box
	Ogre::PlaneBoundedVolume vol;
 
	// Add the first plane vector of the cube
	vol.planes.push_back(Ogre::Plane(topLeft.getPoint(3), topRight.getPoint(3), bottomRight.getPoint(3)));
 
	// Add the second plane vector of the cube
	vol.planes.push_back(Ogre::Plane(topLeft.getOrigin(), topLeft.getPoint(100), topRight.getPoint(100)));
 
	// Add the third plane vector of the cube
	vol.planes.push_back(Ogre::Plane(topLeft.getOrigin(), bottomLeft.getPoint(100), topLeft.getPoint(100)));
 
	// Add the fourth plane vector of the cube
	vol.planes.push_back(Ogre::Plane(bottomLeft.getOrigin(), bottomRight.getPoint(100), bottomLeft.getPoint(100)));
 
	// Add the last plane vector of the cube
	vol.planes.push_back(Ogre::Plane(topRight.getOrigin(), topRight.getPoint(100), bottomRight.getPoint(100)));
 
	// Declare a list of cube volumes that will hold the selection box
	Ogre::PlaneBoundedVolumeList volList;
 
	// Add the cube to the list of volumes
	volList.push_back(vol);
 
	// Sets the volume used to query the scene
	mVolQuery->setVolumes(volList);

	// Execute the query on the scene to look for robots
	Ogre::SceneQueryResult& result = mVolQuery->execute();
 
	// Call function to hide the bounding box of previously selected robots
	deselectObjects();

	// Declare iterator to loop through the query results
	Ogre::SceneQueryResultMovableList::iterator iter;

	// Loop through the query results
	for(iter = result.movables.begin(); iter != result.movables.end(); iter++)
	{
		// Call the function to show the bounding box of the newly selected robot
		selectObject(*iter);
	}
}
//---------------------------------------------------------------------------
void TutorialApplication::deselectObjects()
{
	// Declare an iterator to loop through all of the previously selected robots
	std::list<Ogre::MovableObject*>::iterator iter;
 
	// Loop through all of the previously selected robots
	for(iter = mSelected.begin(); iter != mSelected.end(); iter++)
	{
		// Do not show bounding boxes of this no longer selected robot object
		(*iter)->getParentSceneNode()->showBoundingBox(false);
	}
}
//---------------------------------------------------------------------------
void TutorialApplication::selectObject(Ogre::MovableObject* obj)
{
	// Show the bounding box of this selected robot object
	obj->getParentSceneNode()->showBoundingBox(true);

	// Add the newly selected robot object to the list of selected robots
	mSelected.push_back(obj);
}
//---------------------------------------------------------------------------
void TutorialApplication::swap(float& x, float& y)
{
	// Set temporary variable to the first value 
	float temp = x;

	// Set the first variable to the second value
	x = y;

	// Set the second variable to the temporary value
	y = temp;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
			e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------